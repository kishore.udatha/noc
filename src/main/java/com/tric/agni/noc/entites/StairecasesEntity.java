package com.tric.agni.noc.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "STAIRES")
public class StairecasesEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "ITEAM")
	private String iteam;

	@Column(name = "REQUIRED")
	private String required;

	@Column(name = "REQUIRED_WIDTH")
	private double requiredWidth;

	@Column(name = "PROVIDED")
	private String provided;

	@Column(name = "PROVIDED_WIDTH")
	private String providedwidth;

	@Column(name = "DEFICIT")
	private String deficit;
	
	@Column(name ="NOC_ID")
	private Long nocId;
	
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.DATE)
	private Date updateDate;

}
