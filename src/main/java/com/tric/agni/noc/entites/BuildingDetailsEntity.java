package com.tric.agni.noc.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "BUILDING")
public class BuildingDetailsEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name ="NOC_ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long nocId;

	@Column(name = "BUILDING_NAME")
	private String buildingName;

	@Column(name = "BUILDING_ADDRESS")
	private String buildingAddress;

	@Column(name = "OCCUPIER_NAME")
	private String occupierName;

	@Column(name = "OCCUPIER_ADDRESS")
	private String occupierAddress;

	@Column(name = "FATHER_NAME")
	private String fatherName;

	@Column(name = "MOBILE_NUMBER")
	private String mobilenumber;

	@Column(name = "LANDMARK")
	private String landmark;

	@Column(name = "LATITUDE")
	private String latitude;

	@Column(name = "LONGITUDE")
	private String longitude;

	@Column(name = "DISTRICT")
	private String district;

	@Column(name = "MANDAL")
	private String mandal;

	@Column(name = "FIRE_STATION")
	private String fireStation;

	@Column(name = "FIRE_PREVENTION_WING")
	private boolean firePreventionWing;

	@Column(name = "CONSTRUCTION_YEAR")
	private int yearConstruction;

	@Column(name = "OCCUPANCY_TYPE")
	private String occupancyTyep;

	@Column(name = "BLOCKS")
	private String blocks;

	@Column(name = "HIEGHT")
	private double hieght;
	
	@Column(name = "STATUS")
	private String status;

	@Column(name = "NOC_OBTAINED")
	private boolean nocObtained;
	
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.DATE)
	private Date updateDate;

}
