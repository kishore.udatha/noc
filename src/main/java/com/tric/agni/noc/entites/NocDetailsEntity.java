package com.tric.agni.noc.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "NOCDETAILS")
public class NocDetailsEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="TYPE_OF_NOC_APPLIED")
	private String typeOfNocApplied;
	
	@Column(name="NOC_ISSUEDBY")
	private String nocIssuedBy;
	
	@Column(name="NOC_ISSUED_DATE")
	private Date nocIssuedDate;
	
	@Column(name="NOC_VALID_TILL")
	private Date nocValidTill;
	
	@Column(name="NOC_ISSUED_RC_NUMBER")
	private String nocIssuedRCNumber;
	
	@Column(name="CHALLAN_AMOUNT_PAID")
	private double challanAmountPaid;
	
	@Column(name="CHALANA")
	private String chalana;
	
	@Column(name ="NOC_ID")
	private Long nocId;
	
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.DATE)
	private Date createdDate;
	
	@Column(name = "UPDATE_DATE")
	@Temporal(TemporalType.DATE)
	private Date updateDate;
}
