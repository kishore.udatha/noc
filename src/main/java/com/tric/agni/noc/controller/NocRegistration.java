package com.tric.agni.noc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tric.agni.noc.entites.BuildingDetailsEntity;
import com.tric.agni.noc.entites.NocDetailsEntity;
import com.tric.agni.noc.model.BuildingDetails;
import com.tric.agni.noc.model.NocBlockDetails;
import com.tric.agni.noc.model.NocDetails;
import com.tric.agni.noc.model.Response;
import com.tric.agni.noc.repository.BuildingDetailsRepository;
import com.tric.agni.noc.service.NocRegistrationService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("NOC/")
public class NocRegistration {

	@Autowired
	NocRegistrationService nocRegistrationService;

	@Autowired
	BuildingDetailsRepository buildingDetailsRepository;

	@PostMapping(value = "registration/building")
	public ResponseEntity<Response> saveBuildingDetails(@RequestBody BuildingDetails BuildingDetails) {

		BuildingDetailsEntity entity = nocRegistrationService.saveBuildingDetails(BuildingDetails);
		Response response = new Response();
		if (null != entity) {
			response.setSuccess("True");
			response.setMessage("Building Details saved successfully.");
			response.setId(String.valueOf(entity.getNocId()));
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);

	}

	@PostMapping(value = "registration/nocdetails")
	public ResponseEntity<Response> saveBuildingDetails(@RequestBody NocDetails nocDetails) {

		List<NocDetailsEntity> entity = nocRegistrationService.saveNocDetails(nocDetails);
		Response response = new Response();
		if (null != entity) {
			response.setSuccess("True");
			response.setMessage("NOC Details saved successfully.");
			response.setId(String.valueOf(entity.get(0).getId()));
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);

	}

	@PostMapping(value = "registration/blockdetails")
	public ResponseEntity<Response> saveblockDetails(@RequestBody NocBlockDetails blockDetails) {

		nocRegistrationService.saveBlockDetails(blockDetails);
		Response response = new Response();
		if (null != response) {
			response.setSuccess("True");
			response.setMessage("block Details saved successfully.");
			// response.setId(String.valueOf(entity.get(0).getId()));
		}

		return new ResponseEntity<Response>(response, HttpStatus.OK);

	}

	@GetMapping(value = "{nocid}")
	public ResponseEntity<BuildingDetailsEntity> getNocDetails(@PathVariable("nocid") String nocId) {

		BuildingDetailsEntity response = nocRegistrationService.getNocDetails(nocId);

		return new ResponseEntity<BuildingDetailsEntity>(response, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(value = "/all")
	public ResponseEntity<Iterable<BuildingDetailsEntity>> getAllNocs() {

		Iterable<BuildingDetailsEntity> buildingDetailsEntityList = buildingDetailsRepository.findAll();

		return new ResponseEntity<Iterable<BuildingDetailsEntity>>(buildingDetailsEntityList, HttpStatus.OK);

	}
}
