package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Response {

	private Long nocId;
	private String success;
	private String id;
	private String message;
	

}
