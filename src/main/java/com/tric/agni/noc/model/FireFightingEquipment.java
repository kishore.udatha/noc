package com.tric.agni.noc.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FireFightingEquipment {

	private List<FireSafety> fireSafety;

}
