package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Stairecases {
	private Long nocId;
	private String iteam;
	private String required;
	private double requiredWidth;
	private String provided;
	private String providedwidth;
	private String deficit;

}
