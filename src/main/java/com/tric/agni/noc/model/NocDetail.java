package com.tric.agni.noc.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class NocDetail {
	private Long nocId;
	private String typeOfNocApplied;
	private String nocIssuedBy;
	private Date nocIssuedDate;
	private Date nocValidTill;
	private String nocIssuedRCNumber;
	private double challanAmountPaid;
	private String chalana;

}
