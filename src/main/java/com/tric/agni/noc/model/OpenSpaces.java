package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OpenSpaces {
	private Long nocId;
	private String side;
	private String openSpaceRequired;
	private String openSpaceProvided;
	private String deficit;

}
