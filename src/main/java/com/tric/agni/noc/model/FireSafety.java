package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FireSafety {
	private Long nocId;
	private String fireSafetyMeasures;
	private String required;
	private String provided;
	private String deficit;
}
