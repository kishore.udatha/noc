package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BlockDetail {
	public String blockName;
	private Long nocId;
	public double blockHieght;
	public int floors;

}