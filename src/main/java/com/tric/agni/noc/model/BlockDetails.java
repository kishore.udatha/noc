package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BlockDetails {
	private BlockDetail blockDetails;
	private OpenSpaces openSpaces;
	private Stairecases stairecases;
	private FireSafety fireSafety;

}
