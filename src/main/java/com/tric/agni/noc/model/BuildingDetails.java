package com.tric.agni.noc.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BuildingDetails {

	private String buildingName;
	private String buildingAddress;
	private String occupierName;
	private String occupierAddress;
	private String fatherName;
	private String mobilenumber;
	private String landmark;
	private String latitude;
	private String longitude;
	private String district;
	private String mandal;
	private String fireStation;
	private boolean firePreventionWing;
	private int yearConstruction;
	private String occupancyTyep;
	private String blocks;
	private double hieght;
	private boolean nocObtained;

}
