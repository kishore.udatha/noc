package com.tric.agni.noc.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tric.agni.noc.entites.BlockDetailsEntity;
import com.tric.agni.noc.entites.BuildingDetailsEntity;
import com.tric.agni.noc.entites.FireSafety;
import com.tric.agni.noc.entites.NocDetailsEntity;
import com.tric.agni.noc.entites.OpenSpacesEntity;
import com.tric.agni.noc.entites.StairecasesEntity;
import com.tric.agni.noc.model.BuildingDetails;
import com.tric.agni.noc.model.NocBlockDetails;
import com.tric.agni.noc.model.NocDetails;
import com.tric.agni.noc.repository.BlockDetailsRepository;
import com.tric.agni.noc.repository.BuildingDetailsRepository;
import com.tric.agni.noc.repository.FireSafetyRepository;
import com.tric.agni.noc.repository.NocDetailsRepository;
import com.tric.agni.noc.repository.OpenSpacesRepository;
import com.tric.agni.noc.repository.StairecasesRepository;
import com.tric.agni.noc.service.NocRegistrationService;

@Service
public class NocRegistrationServiceImp implements NocRegistrationService {

	@Autowired
	private BuildingDetailsRepository buildingDetailsRepository;

	@Autowired
	private NocDetailsRepository nocDetailsRepository;

	@Autowired
	private BlockDetailsRepository blockDetailsRepository;

	@Autowired
	private OpenSpacesRepository openSpacesRepository;

	@Autowired
	StairecasesRepository stairecasesRepository;

	@Autowired
	FireSafetyRepository fireSafetyRepository;

	@Override
	public BuildingDetailsEntity saveBuildingDetails(BuildingDetails buildingDetails) {

		if (null != buildingDetails) {
			ModelMapper mapper = new ModelMapper();
			BuildingDetailsEntity entity = mapper.map(buildingDetails, BuildingDetailsEntity.class);
			buildingDetailsRepository.save(entity);
			return entity;

		}
		return null;

	}

	@Override
	public List<NocDetailsEntity> saveNocDetails(NocDetails nocDetails) {
		List<NocDetailsEntity> nocList = new ArrayList<>();
		if (null != nocDetails) {
			nocDetails.getNocDetail().forEach(nocDetail -> {
				ModelMapper mapper = new ModelMapper();
				NocDetailsEntity entity = mapper.map(nocDetail, NocDetailsEntity.class);
				nocDetailsRepository.save(entity);
				nocList.add(entity);
			});

		}
		return nocList;
	}

	@Override
	public void saveBlockDetails(NocBlockDetails nocBlockDetails) {

		if (nocBlockDetails != null) {
			nocBlockDetails.getBlockDetails().forEach(blockDetails -> {

				if (null != nocBlockDetails.getBlockDetails()) {
					BlockDetailsEntity blockDetailsEntity = new BlockDetailsEntity();
					ModelMapper blockMapper = new ModelMapper();
					blockDetailsEntity = blockMapper.map(blockDetails.getBlockDetails(), BlockDetailsEntity.class);
					blockDetailsRepository.save(blockDetailsEntity);
				}

				if (null != blockDetails.getBlockDetails() && null != blockDetails.getOpenSpaces()) {
					OpenSpacesEntity openSpacesEntity = new OpenSpacesEntity();
					ModelMapper openSpacesMapper = new ModelMapper();
					openSpacesEntity = openSpacesMapper.map(blockDetails.getOpenSpaces(), OpenSpacesEntity.class);
					openSpacesRepository.save(openSpacesEntity);
				}

				if (null != blockDetails.getBlockDetails() && null != blockDetails.getStairecases()) {
					StairecasesEntity stairecasesEntity = new StairecasesEntity();
					ModelMapper staireMapper = new ModelMapper();
					stairecasesEntity = staireMapper.map(blockDetails.getStairecases(), StairecasesEntity.class);
					stairecasesRepository.save(stairecasesEntity);
				}

				if (null != blockDetails.getBlockDetails() && null != blockDetails.getFireSafety()) {
					FireSafety fireSafety = new FireSafety();
					ModelMapper staireMapper = new ModelMapper();
					fireSafety = staireMapper.map(blockDetails.getFireSafety(), FireSafety.class);
					fireSafetyRepository.save(fireSafety);
				}
			});

		}
	}

	@Override
	public BuildingDetailsEntity getNocDetails(String nocId) {
		if (StringUtils.isNotBlank(nocId)) {
			return buildingDetailsRepository.findById(Long.parseLong(nocId)).get();
		}
		return null;
	}

}
