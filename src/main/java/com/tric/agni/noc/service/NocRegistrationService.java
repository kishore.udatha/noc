package com.tric.agni.noc.service;

import java.util.List;

import com.tric.agni.noc.entites.BuildingDetailsEntity;
import com.tric.agni.noc.entites.NocDetailsEntity;
import com.tric.agni.noc.model.BuildingDetails;
import com.tric.agni.noc.model.NocBlockDetails;
import com.tric.agni.noc.model.NocDetails;

public interface NocRegistrationService {

	public BuildingDetailsEntity saveBuildingDetails(BuildingDetails buildingDetails);

	public List<NocDetailsEntity> saveNocDetails(NocDetails nocDetails);

	void saveBlockDetails(NocBlockDetails nocBlockDetails);
	
	public BuildingDetailsEntity getNocDetails(String nocId);

}
