package com.tric.agni.noc.repository;

import org.springframework.data.repository.CrudRepository;

import com.tric.agni.noc.entites.FireSafety;

public interface FireSafetyRepository extends CrudRepository<FireSafety, Long>{

}
