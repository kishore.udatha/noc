package com.tric.agni.noc.repository;

import org.springframework.data.repository.CrudRepository;

import com.tric.agni.noc.entites.BlockDetailsEntity;

public interface BlockDetailsRepository extends CrudRepository<BlockDetailsEntity, Long> {

}
