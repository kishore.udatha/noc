package com.tric.agni.noc.repository;

import org.springframework.data.repository.CrudRepository;

import com.tric.agni.noc.entites.BuildingDetailsEntity;

public interface BuildingDetailsRepository extends CrudRepository<BuildingDetailsEntity, Long> {

}
