package com.tric.agni.noc.repository;

import org.springframework.data.repository.CrudRepository;

import com.tric.agni.noc.entites.OpenSpacesEntity;

public interface OpenSpacesRepository extends CrudRepository<OpenSpacesEntity, Long>{

}
