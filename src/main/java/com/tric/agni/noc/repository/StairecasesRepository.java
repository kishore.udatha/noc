package com.tric.agni.noc.repository;

import org.springframework.data.repository.CrudRepository;

import com.tric.agni.noc.entites.StairecasesEntity;

public interface StairecasesRepository extends CrudRepository<StairecasesEntity, Long> {

}
