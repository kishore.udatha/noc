package com.tric.agni.noc.repository;

import org.springframework.data.repository.CrudRepository;

import com.tric.agni.noc.entites.NocDetailsEntity;

public interface NocDetailsRepository extends CrudRepository<NocDetailsEntity, Long> {

}
